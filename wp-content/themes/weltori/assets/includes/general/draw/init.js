var iteracja = 0; 

$( document ).ready(function() {
	var wow = new WOW(
	{
    boxClass:     'wowparalax',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
    	do {
    		var svganimacja = "#paralax-svg-" + iteracja;
    		var svg = new Walkway({
    			selector: svganimacja + ' svg',
    			easing: 'easeInOutCubic',
    			duration: 2100
    		}).draw();
    		iteracja++;
    	}
    	while (iteracja < 10);
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
}