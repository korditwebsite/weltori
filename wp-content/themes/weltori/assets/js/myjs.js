//Hamburger menu
$(document).ready(function(){
	$(".hamburger").click(function(){
		$(this).toggleClass("is-active");
		$("nav.blur-menu").toggleClass("active");
		$("nav.slide-menu").toggleClass("active");
	});
});
$(".hamburger").click(function(){
	$(this).toggleClass("is-active");
});

$(".perspective").click(function(){
	$("body").toggleClass("disable-scroll");
	$("nav").toggleClass("active");
});
//affix
$(window).on('scroll', function (event) {
	var scrollValue = $(window).scrollTop();
	if (scrollValue > 120) {
		$('#header-3').addClass('affix');
	} else{
		$('#header-3').removeClass('affix');
	}
});
$(window).on('scroll', function (event) {
	var scrollValue = $(window).scrollTop();
	if (scrollValue > 120) {
		$('#header-4 .affix-container').addClass('affix');
	} else{
		$('#header-4 .affix-container').removeClass('affix');
	}
});

$(document).ready(function(){
	$(".choose .half:nth-child(1)").mouseenter(function(){
		$(this).addClass("active");
		$(this).removeClass("smaller");
		$(".choose .half:nth-child(1) .content").addClass("active");
		$(".choose .half:nth-child(2) .content").removeClass("active");
		$(".choose .half:nth-child(2)").removeClass("active");
		$(".choose .half:nth-child(2)").addClass("smaller");
	});
});
$(document).ready(function(){
	$(".choose .half:nth-child(2)").mouseenter(function(){
		$(this).addClass("active");
		$(this).removeClass("smaller");
		$(".choose .half:nth-child(2) .content").addClass("active");
		$(".choose .half:nth-child(1) .content").removeClass("active");
		$(".choose .half:nth-child(1)").addClass("smaller");
		$(".choose .half:nth-child(1)").removeClass("active");
	});
});

$(".hamburger").click(function(){ 
	$(this).toggleClass("is-active");
});

$().siblings("ul.dropdown").toggleClass("active");

$('.loop').owlCarousel({
	center: true,
	items:1,
	loop:true,
	margin:10,
	nav: true,
	responsive:{
		600:{
			items:3
		}
	}
});
$(".section-owl-carousel .owl-nav").removeClass('disabled');
$(document).ready(function(){
	$(".owl-carousel").owlCarousel({center:true});

});
$(function() {                       //run when the DOM is ready
  $(".open-login-popup").click(function() {  //use a class, since your ID gets mangled
    $('.popup-login').addClass("show");      //add the class to the clicked element
});
});
$(function() {                       
	$(".close-login-popup").click(function() { 
		$('.popup-login').removeClass("show");      
	});
});