<?php if ( have_rows( 'ustawnienia_footera', 'option' ) ) : 
	while ( have_rows( 'ustawnienia_footera', 'option' ) ) : the_row();
		$linkfb = get_sub_field( 'link_do_facebooka' );
		$widgetfb = get_sub_field( 'wyswietl_widget_z_boku' ); 
		$wyborfootera = get_sub_field( 'wyglad_footera' ); 
	endwhile; endif; ?>
	<footer id="footer-<?php echo $wyborfootera; ?>">
		<?php get_template_part( 'templates/footer/init');?>
	</footer>

	<?php wp_footer(); ?>
</body>
</html>
