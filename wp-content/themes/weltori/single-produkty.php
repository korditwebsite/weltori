<?php get_header(2); ?>
<main class="single-produkty">
	<section class="presentation-logotype presentation-slider">
		<div class="section-background-image" style="background-image:url(<?php echo id_image_url("tlo_produktu", "full") ?>);">
		</div>
	</div>
	<div class="container">
		<div class="position-content-box">
			<div class="content-logotype">

				<div class="checked-default">
					<h3 class="wow fadeIn"><?php echo get_the_title(); ?></h3>
				</div>
				<div class="checked-text">
					<a href="#about">
						<img src="/wp-content/uploads/2019/12/arrow.svg" alt="" class="arow wow bounce infinite">
					</a>
					<div class="text wow fadeIn"><?php the_field( 'podtytul_slider' ); ?></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="details" id="about">
	<div class="bg-theme"></div>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h6><?php the_field( 'podtytul' ); ?></h6>
			</div>
			<div class="col-xl-12">
				<h3><?php the_field("wiekszy_tytul"); ?></h3>
			</div>
			<div class="col-xl-6 col-12 wow fadeInLeft p-relative">
				
				<div class="description">
					<?php the_field("opis"); ?>
				</div>
			</div>
			<div class="col-xl-6 col-12 wow fadeInRight">				
				<?php
				the_post_thumbnail( 'post-thumbnail', '' );
				?>
			</div>
		</div>
	</div>
</section> 
</main>
<?php get_footer(); ?>