<!DOCTYPE html>
<html class="mt-0" <?php language_attributes(); ?>>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="keywords" content="<?php the_field("slowa_kluczowe", "options"); ?>" />
	<style type="text/css">
		@font-face {
			font-family: "Didot";
			src: url("/wp-content/themes/weltori/assets/fonts/didot.ttf");
		}
		@font-face {
			font-family: "Acenir";
			src: url("/wp-content/themes/weltori/assets/fonts/Avenir.ttc");
		}
	</style>
	<?php if (is_page_template( 'single.php' )): ?>
	<?php endif ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class( 'class-name' ); ?>>
	<?php if (is_user_logged_in()) : ?>
		<a class="logout" href="/wp-login.php?action=logout">➾ <span>wyloguj</span></a>
	<?php endif; ?>
	<?php
	if ( have_rows( 'header-settings', 'option' ) ) : 
		while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
			$wyborheadera = get_sub_field( 'header_sekcja' ); 
		endwhile;
	endif;
	?>
	<header id="header-<?php echo $wyborheadera; ?>">
		<?php get_template_part( 'templates/header/init');?>
	</header>