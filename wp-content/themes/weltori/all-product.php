<?php
/**
* Template Name: Wszystkie produkty
*/
?>
<?php get_header(2); ?>
<section class="presentation-logotype presentation-slider">
	<div class="section-background-image" style="background-image:url(<?php echo
		get_the_post_thumbnail_url() ?>);">
	</div>
	<div class="section-background-image wow fadeIn" style="background-image: url(<?php the_field( 'background-1' ); ?>);">
	</div>
	<div class="container">
		<div class="position-content-box">
			<div class="content-logotype">

				<div class="checked-default">
					<h3 class="wow fadeIn"><?php echo get_the_title(); ?></h3>
				</div>
				<div class="number wow jackInTheBox">
					<?php the_field( 'number' ); ?>
				</div>
				<div class="sygnet wow jackInTheBox">
					<?php echo id_image("sygnet", "full") ?>
				</div>
				<div class="checked-text">
					<a href="#more-service">
						<img src="/wp-content/uploads/2019/12/arrow.svg" alt="" class="arow wow bounce infinite">
					</a>
					<div class="text wow fadeIn"><?php the_field( 'podtytul_slider' ); ?></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content-service" id="more-service">
	<div class="background-custom">

	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-6 wow fadeInLeft">
				<?php echo id_image("grafika", "full") ?>
			</div>
			<div class="col-xl-6">
				<div class="mini-title wow fadeInRight">
					<div class="number">
						<?php the_field( 'number_bg' ); ?>
					</div>
					<div class="inner-title">
						<h3><?php the_field( 'minitytul' ); ?></h3>
					</div>
				</div>
				<div class="before-content wow fadeInRight">
					<p><?php the_field( 'podtytul' ); ?></p>
				</div>
				<div class="more-content wow fadeInRight">
					<?php the_field( 'podpis' ); ?>
				</div>
			</div>
		</div>
	</div>
</section> 
<section class="section-owl-carousel wow fadeInDown">
	<div class="container">
		<div class="loop owl-carousel owl-theme owl-loaded owl-drag">
			<div class="owl-stage-outer">
				<div class="owl-stage" style="transition: all 0.25s ease 0s; width: 5880px; transform: translate3d(-1102px, 0px, 0px);">
					<?php 
					$args = array( 'post_type' => 'Produkty',
						'posts_per_page' => -1, 
						'orderby' => 'date',
						'order' => 'DESC',
					);
					$the_query = new WP_Query( $args ); 
					?>
					<?php if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<div class="owl-item active" style="width: 235px; margin-right: 10px;">
								<div class="item">
									<div class="thumbnail">
										<?php the_post_thumbnail(); ?>
									</div>
									<h3><?php echo get_the_title(); ?></h3>
									<div class="more-info">
										<p><?php the_field( 'krotki_opis' ); ?></p>
										<a href="<?php the_permalink(); ?>">więcej</a>
									</div>
								</div>
							</div>
						<?php endwhile; else:  ?>
						<p><?php _e( 'Posry w trakcie uzupełniania.' ); ?></p>
					<?php  endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>