<?php
//Inicjacja plików motywu	

add_action('acf/render_field_settings', 'my_admin_only_render_field_settings');

add_action('admin_init', 'disable_dashboard');
function disable_dashboard() {
	if (!is_user_logged_in()) {
		return null;
	}
	if (!current_user_can('administrator') && is_admin()) {
		wp_redirect(home_url()); exit;
	}
}

function my_admin_only_render_field_settings( $field ) {
	
	acf_render_field_setting( $field, array(
		'label'			=> __('Admin Only?'),
		'instructions'	=> '',
		'name'			=> 'admin_only',
		'type'			=> 'true_false',
		'ui'			=> 1,
	), true);
	
}
add_action('set_current_user', 'cc_hide_admin_bar');
function cc_hide_admin_bar() {
	if (!current_user_can('edit_posts')) {
		show_admin_bar(false);
	}
}

add_action('admin_init', 'disable_admin_bar'); function disable_admin_bar() { if (current_user_can('subscriber')) { show_admin_bar(false); } }

function my_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/assets/sass/_general.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
//Dodawanie wsparcia dla motywu
get_template_part( '/assets/includes/support'); 

//Dodawanie wsparcia dla CPT
get_template_part( '/assets/includes/cpt'); 

//Dodawanie widgetów
get_template_part( '/assets/includes/widget'); 

//Dodawanie rozmiarów miniaturek
get_template_part( '/assets/includes/size-thumbnail'); 

//Tworzenie automatycznej struktury nawigacji
get_template_part( '/assets/includes/walker-nav-menu'); 

// Register WordPress nav menu
register_nav_menu('top', 'Primary menu');
register_nav_menu('top2', 'Secondary menu');
register_nav_menu('footer', 'Footer menu');
register_nav_menu('menu', 'Karta menu');

function twentynineteen_scripts() {
	wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/assets/js/jquery.js', array(), '3.3.1', true );
	wp_enqueue_script( 'bundle',  get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'Bootstrap',  get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'apear', get_template_directory_uri() . '/assets/js/apear.js',  array(), '1.0.0', true);
	wp_enqueue_script( 'Main', get_template_directory_uri() . '/assets/js/initial.js', array(), '1.0.0', true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl.js', array(), '1.0.0', true );
	wp_enqueue_script( 'my-js', get_template_directory_uri() . '/assets/js/myjs.js', array(), '1.0.0', true );	
}
add_action( 'wp_enqueue_scripts', 'twentynineteen_scripts' );

add_action( 'wp_enqueue_scripts', 'wpse_my_style' );
function wpse_my_style(){
	wp_enqueue_style( 'normalize', get_template_directory_uri() . '/assets/css/normalize.css' );
	wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css' );
	wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/sass/main-theme.css' );
	wp_enqueue_style( 'owl-css', get_template_directory_uri() . '/assets/sass/owl.css' );
}  


add_filter('style_loader_tag', 'codeless_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'codeless_remove_type_attr', 10, 2);
function codeless_remove_type_attr($tag, $handle) {
	return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}

/* Main redirection of the default login page */
function redirect_login_page() {
	$login_page  = home_url('/login/');
	$page_viewed = basename($_SERVER['REQUEST_URI']);

	if($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($login_page);
		exit;
	}
}
add_action('init','redirect_login_page');

/* Where to go if a login failed */
function custom_login_failed() {
	$login_page  = home_url('/login/');
	wp_redirect($login_page . '?login=failed');
	exit;
}
add_action('wp_login_failed', 'custom_login_failed');

/* Where to go if any of the fields were empty */
function verify_user_pass($user, $username, $password) {
	$login_page  = home_url('/login/');
	if($username == "" || $password == "") {
		wp_redirect($login_page . "?login=empty");
		exit;
	}
}
add_filter('authenticate', 'verify_user_pass', 1, 3); 

/* What to do on logout */
function logout_redirect() {
	$login_page  = home_url('/login/');
	wp_redirect($login_page . "?login=false");
	exit;
}
add_action('wp_logout','logout_redirect');
function start_session() {
	if(!session_id()) {
		session_start();
	}
}
add_action('init', 'start_session', 1);

// get  referer url and save it 
function redirect_url() {
	if (! is_user_logged_in()) {
		$_SESSION['referer_url'] = wp_get_referer();
	} else {
		session_destroy();
	}
}
add_action( 'template_redirect', 'redirect_url' );
