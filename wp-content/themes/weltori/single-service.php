<?php
get_header(2);
?>
<div class="popup-login">
	<div class="close-login-popup">×</div>
	<?php echo do_shortcode( '[lrm_form default_tab="login"]' ); ?>
</div>
<main class="single-produkty-files">
	<section class="presentation-logotype presentation-slider">
		<div class="section-background-image" style="background-image:url(<?php echo
			get_the_post_thumbnail_url() ?>);">

			<style type="text/css">
				.sygnet-postion {
					<?php 
					if ( have_rows( 'polozenie' ) ) :  while ( have_rows( 'polozenie' ) ) : the_row();
//bottom distance 
						if ( have_rows( 'dol' ) ) :  while ( have_rows( 'dol' ) ) : the_row();
							$wartosc = get_sub_field( 'wartosc' );
							if ($wartosc) {	
								$miara = get_sub_field( 'miara' );
							}

							$calosc = $wartosc . $miara;
							if ($calosc && $wartosc) {
								echo "bottom:" . $calosc . ";";
							}
						endwhile; endif;
//top distance
						if ( have_rows( 'top' ) ) :  while ( have_rows( 'top' ) ) : the_row();
							$wartosc = get_sub_field( 'wartosc' );
							if ($wartosc) {
								$miara = get_sub_field( 'miara' );
							}

							$calosc = $wartosc . $miara;
							if ($calosc && $wartosc) {
								echo "top:" . $calosc . ";";
							}
						endwhile; endif;

						if ( have_rows( 'lewa' ) ) :  while ( have_rows( 'lewa' ) ) : the_row();
							$wartosc = get_sub_field( 'wartosc' );
							if ($wartosc ) {	
								$miara = get_sub_field( 'miara' );
							}

							$calosc = $wartosc . $miara;
							if ($calosc && $wartosc) {
								echo "left:" . $calosc . ";";
							}
						endwhile; endif;

						if ( have_rows( 'prawa' ) ) :  while ( have_rows( 'prawa' ) ) : the_row();
							$wartosc = get_sub_field( 'wartosc' );
							if ($wartosc) {	
								$miara = get_sub_field( 'miara' );
							}

							$calosc = $wartosc . $miara;
							if ($calosc && $wartosc) {
								echo "right:" . $calosc . ";";
							}
						endwhile; endif;
					endwhile; endif; ?>
				}
			</style>
			<div class="sygnet-postion">
				<?php echo id_image("sygnet", "full") ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="position-content-box">
			<div class="content-logotype">

				<div class="checked-default">
					<h3 class="wow fadeIn"><?php echo get_the_title(); ?></h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content-product" id="content-product">
	<h3 class="absoulte-this"><?php the_field( 'subtytul' ); ?></h3>
	<div class="bg-color" style="background-color: <?php the_field( 'kolor' ); ?>"></div>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h2><?php the_field( 'subtytul' ); ?></h2>
			</div>
			<div class="col-xl-6">
				<div class="opis">
					<?php the_field( 'opis' ); ?>
				</div>
				<?php if (!is_user_logged_in()) : ?>
					<div class="button-download">
						<h4>pliki do pobrania</h4>
						<div class="href-left">
							<a class="open-login-popup" href="#content-product">zaloguj się</a>
						</div>
					</div>
				<?php endif ?>
			</div>
			<div class="col-xl-6">
				<div class="thumbnail">
					<?php echo id_image("dodatkowa_grafika", "full") ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if (is_user_logged_in()) : ?>
	<?php $current_user = get_current_user_id(); ?>
	<?php
	$current_select = get_field('dostep_do_plikow', 'user_' . $current_user);
	?>
	<?php if (in_array("1", $current_select)) : 
		global $post;
		if( $post->ID == 473) : ?>
			<section class="download-files">
				<div class="container">
					<div class="col-xl-12">
						<h2>Pliki do pobrania:</h2>
					</div>
					<?php if ( have_rows( 'pliki_do_pobrania' ) ) : while ( have_rows( 'pliki_do_pobrania' ) ) : the_row(); ?>
						<div class="single-item">
							<div class="row">
								<div class="col-9">
									<?php the_sub_field( 'etykieta' ); ?>
								</div>
								<div class="col-3">
									<a target="_blank" href="<?php the_sub_field( 'plik' ); ?>">
										pobierz <img src="/wp-content/uploads/2019/12/down.png">
									</a>
								</div>
							</div>
						</div>
					<?php endwhile;  endif; ?>
				</div>
			</section>
		<?php endif;endif; ?>
		<?php if (in_array("2", $current_select)) : 
			global $post;
			if( $post->ID == 471) : ?>
				<section class="download-files">
					<div class="container">
						<div class="col-xl-12">
							<h2>Pliki do pobrania:</h2>
						</div>
						<?php if ( have_rows( 'pliki_do_pobrania' ) ) : while ( have_rows( 'pliki_do_pobrania' ) ) : the_row(); ?>
							<div class="single-item">
								<div class="row">
									<div class="col-9">
										<?php the_sub_field( 'etykieta' ); ?>
									</div>
									<div class="col-3">
										<a target="_blank" href="<?php the_sub_field( 'plik' ); ?>">
											pobierz <img src="/wp-content/uploads/2019/12/down.png">
										</a>
									</div>
								</div>
							</div>
						<?php endwhile;  endif; ?>
					</div>
				</section>
			<?php endif;endif; ?>
			<?php if (in_array("3", $current_select)) : 
				global $post;
				if( $post->ID == 469) : ?>
					<section class="download-files">
						<div class="container">
							<div class="col-xl-12">
								<h2>Pliki do pobrania:</h2>
							</div>
							<?php if ( have_rows( 'pliki_do_pobrania' ) ) : while ( have_rows( 'pliki_do_pobrania' ) ) : the_row(); ?>
								<div class="single-item">
									<div class="row">
										<div class="col-9">
											<?php the_sub_field( 'etykieta' ); ?>
										</div>
										<div class="col-3">
											<a target="_blank" href="<?php the_sub_field( 'plik' ); ?>">
												pobierz <img src="/wp-content/uploads/2019/12/down.png">
											</a>
										</div>
									</div>
								</div>
							<?php endwhile;  endif; ?>
						</div>
					</section>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
	</main>

	<?php get_footer(); ?>


