<?php 
if (get_field( 'check_template')  === "1" ) {
	get_template_part( 'templates/single-post/service/service-1');
} 
elseif (get_field( 'check_template' ) === "2") {
	get_template_part( 'templates/single-post/service/service-2');
} 
elseif (get_field( 'check_template' ) === "3") {
	get_template_part( 'templates/single-post/service/service-3');
} 
else {
	echo 'Wystąpił błąd działania motywu. Skontaktuj się z adminem na adres <a href="mailto:wordpress@kordit.pl">wordpress@kordit.pl</a>';
}