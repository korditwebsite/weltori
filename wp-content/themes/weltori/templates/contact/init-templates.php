<?php
//inicjacja ustawień
if( get_row_layout() == 'podstawowy_kontakt' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		include('wp-content/themes/' . wp_get_theme() . '/templates/contact/contact-1/contact.php');
	endwhile; endif;



elseif( get_row_layout() == 'kontakt_dzialowy' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		include('wp-content/themes/' . wp_get_theme() . '/templates/contact/contact-3/contact.php');
	endwhile; endif;


elseif( get_row_layout() == 'offer' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	include('wp-content/themes/' . wp_get_theme() . '/templates/basic/offer/init.php');
endif
?>
