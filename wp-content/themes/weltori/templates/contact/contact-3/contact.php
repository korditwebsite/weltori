<section class="kontakt-3" style="background: <?php echo $hn_bgcolor ?>">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="title-section">
					<h3><?php the_sub_field( 'tytul' ); ?></h3>
				</div>
			</div>
		</div>
		<div class="row mb-5">
			<?php if ( have_rows( 'dzial' ) ) :  while ( have_rows( 'dzial' ) ) : the_row(); ?>
				<div class="single-dzial <?php echo $hn_rwd; ?>">
					<h3><?php the_sub_field( 'nazwa_dzialu' ); ?></h3>
					<?php if ( have_rows( 'dzial_szczegoly' ) ) : while ( have_rows( 'dzial_szczegoly' ) ) : the_row(); ?>
						<div class="single-dzial-inner">
							<address>
								<?php the_sub_field( 'informacje' ); ?>
							</address>
							<div class="container-mobile">
								<?php if ( have_rows( 'numery_telefonow' ) ) : while ( have_rows( 'numery_telefonow' ) ) : the_row(); ?>
									<a href="tel:<?php the_sub_field( 'numer' ); ?>">T: <?php the_sub_field( 'etykieta' ); ?></a>
								<?php endwhile; endif; ?>
							</div>
							<div class="container-fax">
								<?php if ( have_rows( 'fax' ) ) : while ( have_rows( 'fax' ) ) : the_row(); ?>
									<a href="tel:<?php the_sub_field( 'numer' ); ?>">F: <?php the_sub_field( 'etykieta' ); ?></a>
								<?php endwhile; endif; ?>
							</div>
							<div class="container-mail">
								<?php if ( have_rows( 'adresy_email' ) ) : while ( have_rows( 'adresy_email' ) ) : the_row(); ?>
									<a href="mailto:<?php the_sub_field( 'etykieta' ); ?>">E: <?php the_sub_field( 'etykieta' ); ?></a>
								<?php endwhile; endif; ?>
							</div>
						</div>
					<?php endwhile; endif; ?>
					<?php if ( have_rows( 'Sprzedawca' ) ) : while ( have_rows( 'Sprzedawca' ) ) : the_row(); ?>
						<div class="single-sprzedawca">
							<div class="single-dzial-inner">
								<address>
									<?php the_sub_field( 'imie_i_nazwisko' ); ?>
								</address>
								<div class="container-mobile">
									<a href="tel:<?php the_sub_field( 'telefon' ); ?>">T: <?php the_sub_field( 'telefon' ); ?></a>
								</div>
								<div class="container-mail">
									<a href="mailto:<?php the_sub_field( 'email' ); ?>">E: <?php the_sub_field( 'email' ); ?></a>
								</div>
							</div>
						</div>
					<?php endwhile; endif; ?>

				</div>

			<?php endwhile; endif; ?>
		</div>
		<div class="row">
			<div class="container-form">
				<?php echo do_shortcode(get_sub_field( 'formularz' )); ?>
			</div>
		</div>
		<div class="row">
			<map name="adress">
				<?php the_sub_field( 'google_maps' ); ?>
			</map>
		</div>
	</div>
</section> 