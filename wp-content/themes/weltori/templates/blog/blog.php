<?php get_header(); ?>
<main id="blog">
	<section class="blog-page">
		<div class="container">
			<div class="row">
				<div class="col-xl-9">
					<div class="container-all-post">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
							$value = get_the_title();
							$str = iconv('UTF-8','ASCII//TRANSLIT',$value);
							$str = preg_replace("/[^a-z0-9- ]+/i", "", $str);
							$str = preg_replace('!\s+!', ' ', $str);
							$str = str_replace(" ", "-", $str);
							$stripped = strtolower($str);
							?>
							<article title="<?php the_title(); ?>" class="single-blog-post class-<?php echo $stripped; ?>" >
								<div class="post-container">
									<div class="content-text">
										<?php  
										the_content();
										?>
									</div>
								</div>
							</article>
						<?php endwhile; else : ?>
						<p><?php esc_html_e( 'Wpisy w trakcie przygotowania, zapraszamy wkrótce!' ); endif ?></p>
					</div>
				</div>
				<div class="col-xl-3">
					<aside>
						<?php get_sidebar(); ?>
					</aside>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>

