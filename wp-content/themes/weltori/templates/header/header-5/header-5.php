<?php 
if ( have_rows( 'header-settings', 'option' ) ) : 
	while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
		$nbrbtn = get_sub_field( 'button_navi' );
		$wyborheadera = get_sub_field( 'header_sekcja' ); 
	endwhile;
endif;
?>
<div class="logotype">
	<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo = wp_get_attachment_image_src( $custom_logo_id , 'small-logo' );
	if ( has_custom_logo() ) {
		echo '<img class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
	} else {
		echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
	}
	if ( have_rows( 'header-settings', 'option' ) ) : 
		while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
			$nbrbtn = get_sub_field( 'button_navi' );
			$wyborheadera = get_sub_field( 'header_sekcja' ); 
		endwhile;
	endif;
	?>
</div>
<div class="open-navigation">
	<div class="hamburger perspective" id="hamburger-<?php echo $nbrbtn; ?>">
		<span class="line"></span>
		<span class="line"></span>
		<span class="line"></span>
	</div>
</div>
<nav>
	<div class="section-logo">
		<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
		$logo = wp_get_attachment_image_src( $custom_logo_id , 'small-logo' );
		if ( has_custom_logo() ) {
			echo '<img class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
		} else {
			echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
		}
		if ( have_rows( 'header-settings', 'option' ) ) : 
			while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
				$nbrbtn = get_sub_field( 'button_navi' );
				$wyborheadera = get_sub_field( 'header_sekcja' ); 
			endwhile;
		endif;
		?>
	</div>
	<?php
	wp_nav_menu([
		'menu'            => 'top',
		'theme_location'  => 'top',
		'container'       => 'div',
		'container_class' => 'mobile-none navi-container-5',
		'menu_id'         => false,
		'menu_class'      => 'navbar-nav',
		'depth'           => 3,
		'fallback_cb'     => 'bs4navwalker::fallback',
		'walker'          => new bs4navwalker()
	]);
	?>
</nav>