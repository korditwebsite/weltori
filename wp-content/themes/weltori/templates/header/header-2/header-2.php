<?php 
$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
?>

<nav class="slide-menu">
	<div class="container-navi">
		<div class="inner-container-menu">
			<div class="first-menu">
				<?php
				wp_nav_menu([
					'menu'            => 'top',
					'theme_location'  => 'top',
					'container'       => 'div',
					'container_class' => 'blur-menu__nawigacja-menu',
					'menu_class'      => 'navbar-nav',
					'depth'           => 2,
					'fallback_cb'     => 'bs4navwalker::fallback',
					'walker'          => new bs4navwalker(),
				]);

				if ( have_rows( 'header-settings', 'option' ) ) : 
					while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
						$nbrbtn = get_sub_field( 'button_navi' );
						$wyborheadera = get_sub_field( 'header_sekcja' ); 
					endwhile;
				endif;
				?>
			</div>
			<div class="second-menu">
				<span>Oferta:</span>
				<?php wp_nav_menu([
					'menu'            => 'top2',
					'theme_location'  => 'top2',
					'container'       => 'div',
					'container_class' => 'blur-menu__nawigacja-menu',
					'menu_class'      => 'navbar-nav',
					'depth'           => 3,
					'fallback_cb'     => 'bs4navwalker::fallback',
					'walker'          => new bs4navwalker(),
				]); ?>
				<ul>
					<?php
					// $args = array(
					// 	'post_type'   => "service",
					// 	'post_status' => 'publish'
					// );

					// $testimonials = new WP_Query( $args );
					// if( $testimonials->have_posts() ) :
					// 	while( $testimonials->have_posts() ) :
					// 		$testimonials->the_post();
					?>
							<!-- <li>
								<a  title="<?php the_title(); ?>" href="<?php echo get_permalink(); ?>"><?php the_title(); ?> </a>
							</li> -->
							<?php
						// endwhile; endif;
						// wp_reset_postdata();
							?>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="header-blur-container">
			<div class="container">
				<div class="row">
					<div class="col-6">
						<div class="container-logotype">
							<a href="/">
								<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
								$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
								if ( has_custom_logo() ) {
									echo '<img alt="logotyp" class="img-fluid logotype" src="'. esc_url( $logo[0] ) .'">';
								} else {
									echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
								} ?>
							</a>
						</div>
					</div>
					<div class="col-6">
						<div class="open-navigation">
							<div class="hamburger" id="hamburger-<?php echo $nbrbtn; ?>">
								<span class="line"></span>
								<span class="line"></span>
								<span class="line"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>