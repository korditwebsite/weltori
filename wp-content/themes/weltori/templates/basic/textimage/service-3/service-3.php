<section class="service-3" style="background: <?php echo $hn_bgcolor ?>">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground ?>); opacity: <?php echo $hn_opacity; ?>"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="row">
			<div class="<?php echo $hn_service_size_image; ?> wow fadeInUp">
				<div class="description">
					<?php the_sub_field("tekst"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="czekolada czekolada-1 wow fadeInLeft">
		<?php
		echo wp_get_attachment_image( get_sub_field('grafika'), "about-small-1", "", array( "class" => "w-100 img-fluid") );
		?>
	</div>
	<div class="czekolada czekolada-2 wow fadeInUp">
		<?php
		echo wp_get_attachment_image( get_sub_field('grafika_2'), "about-small-1", "", array( "class" => "w-100 img-fluid") );
		?>
	</div>
</section> 