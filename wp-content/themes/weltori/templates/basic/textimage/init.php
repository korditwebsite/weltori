<?php
if ( have_rows( 'ustawienia' ) ) : while ( have_rows( 'ustawienia' ) ) : the_row(); 
	$hn_service_text_desktop = get_sub_field( 'desktop_tekst' );
	$hn_service_text_tablet = get_sub_field( 'tablet_tekst' );
	$hn_service_text_mobile = get_sub_field( 'Mobile_tekst' );
endwhile; endif;

$hn_service_size_image = " ";
$hn_service_size_text = " ";
$hn_service_size_image = "col-xl-" . $hn_desktop . " col-md-" . $hn_tablet . " col-" . $hn_mobile;
$hn_service_size_text = "col-xl-" . $hn_service_text_desktop . " col-md-" . $hn_service_text_tablet . " col-" . $hn_service_text_mobile;
$x = $hn_ulozenie;
if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
	add_css_theme("textimage-" . $x, '/templates/basic/textimage/service-' . $x . '/scss/style.css');
	include('wp-content/themes/' . wp_get_theme() . '/templates/basic/textimage/service-' . $x . "/service-" . $x . ".php");
	//get_template_part( );
endwhile; endif;