<?php
//inicjacja ustawień
$hn_url_templates = get_template_directory_uri() . "/templates/basic/";
if( get_row_layout() == 'grafika_z_tekstem' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	include('wp-content/themes/' . wp_get_theme() . '/templates/basic/textimage/init.php');



elseif( get_row_layout() == 'textbox' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		include('wp-content/themes/' . wp_get_theme() . '/templates/basic/textbox/textbox.php');
	endwhile; endif;


elseif( get_row_layout() == 'offer' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	include('wp-content/themes/' . wp_get_theme() . '/templates/basic/offer/init.php');
endif
?>
