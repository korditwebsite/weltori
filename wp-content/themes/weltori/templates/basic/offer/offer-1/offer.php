<section class="oferta-1" id="oferta" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="<?php echo $hn_size_container; ?>">
		<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
		<div class="row">
			<?php $x = 01; $time = 250; if( have_rows('pojedyncza_oferta') ):  while ( have_rows('pojedyncza_oferta') ) : the_row();  ?>
			<div class="<?php echo $hn_rwd; ?> wow fadeInUp single-item" data-wow-delay="<?php echo $time; ?>ms">
				<div class="offer-wrapper">
					<div class="custom-bg"></div>
					<div class="offer-box">
						<div class="offer-box-wrapper">
							<div class="title-section">
								<span class="number">0<?php echo $x; ?></span>
								<h3><?php the_sub_field("tytul"); ?></h3>
							</div>
							<div class="thumbnail">
								<img alt="Oferta Weltori" src="<?php echo the_sub_field( 'grafika' ); ?>">
							</div>
							<div class="text-container">
								<p><?php the_sub_field("opis"); ?></p>
								<?php if (get_sub_field("link")): ?>
									<div class="href-left">
										<a href="<?php the_sub_field("link"); ?>">
											więcej 
											<svg 
											xmlns="http://www.w3.org/2000/svg"
											xmlns:xlink="http://www.w3.org/1999/xlink"
											width="8px" height="14px">
											<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
											d="M7.926,7.206 L7.924,7.208 L7.925,7.210 L7.139,8.008 L7.137,8.006 L1.609,13.609 L0.820,12.809 L6.347,7.205 L0.843,1.625 L1.630,0.828 L7.134,6.408 L7.136,6.405 L7.926,7.206 Z"/>
										</svg>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $x = $x + 1; $time = $time + 250; endwhile; else : endif; ?>
	</div>
</div>
</section>