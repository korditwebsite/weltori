<?php if ( have_rows( 'ustawnienia_footera', 'option' ) ) : 
	while ( have_rows( 'ustawnienia_footera', 'option' ) ) : the_row();
		$linkfb = get_sub_field( 'link_do_facebooka' );
		$widgetfb = get_sub_field( 'wyswietl_widget_z_boku' ); 
		$wyborfootera = get_sub_field( 'wyglad_footera' ); 
	endwhile; endif; ?>
	<?php if( have_rows('ustawnienia_footera', 'option') ): while( have_rows('ustawnienia_footera', 'option') ): the_row(); ?>
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					if ( has_custom_logo() ) {
						echo '<img alt="logotyp" class="img-fluid logotype" src="'. esc_url( $logo[0] ) .'">';
					} else {
						echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
					} ?>
				</div>
				<div class="col-xl-3">

				</div>
				<div class="col-xl-2">
					<?php
					wp_nav_menu([
						'menu'            => 'top',
						'theme_location'  => 'top',
						'container'       => 'div',
						'container_class' => 'blur-menu__nawigacja-menu',
						'menu_class'      => 'navbar-nav',
						'depth'           => 2,
						'fallback_cb'     => 'bs4navwalker::fallback',
						'walker'          => new bs4navwalker(),
					]);
					?>
				</div>
				<div class="col-xl-3">
					<span>Oferta:</span>
					<ul>
						<?php
						$args = array(
							'post_type'   => "service",
							'post_status' => 'publish'
						);

						$testimonials = new WP_Query( $args );
						if( $testimonials->have_posts() ) :
							while( $testimonials->have_posts() ) :
								$testimonials->the_post();
								?>
								<li>
									<a  title="<?php the_title(); ?>" href="<?php echo get_permalink(); ?>"><?php the_title(); ?> </a>
								</li>
								<?php
							endwhile; endif;
							wp_reset_postdata();
							?>
						</ul>
					</div>
					<div class="col-xl-4">
						<div class="row">
							<div class="col-xl-12">
								<address>
									<?php the_sub_field( 'informacje' ); ?>
								</address>
							</div>
							<?php if ( have_rows( 'telefony' ) ) : while ( have_rows( 'telefony' ) ) : the_row(); ?>
								<div class="col-xl-6">
									<a href="<?php the_sub_field( 'href' ); ?>"><?php the_sub_field( 'telefon' ); ?></a>
								</div>
							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
				<div class="credits">
					<div class="row">
						<hr>
						<div class="col-6">
							<p>Wszystkie prawa zastrzeżone 2020</p>
						</div>
						<div class="col-6">
							<p>Projekt i realizacja: Emtigo Design</p>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; endif; ?>