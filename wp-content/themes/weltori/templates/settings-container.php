<?php
$hn_imagebackground = " ";
$hn_opacity = " ";
$hn_bgcolor = "";
$hn_fcolor = " ";
$hn_scolor = " ";
if ( have_rows( 'ustawienia_styli' ) ) : while ( have_rows( 'ustawienia_styli' ) ) : the_row();
	if (id_subimage_url("tlo_graficzne", "full")) {
		$hn_imagebackground = id_subimage_url("tlo_graficzne", "full") ;
	}
	if (get_sub_field( 'przezroczystosc' )) {
		$hn_opacity = get_sub_field( 'przezroczystosc' );
	}
	if (get_sub_field( 'kolor_tla' )) {
		$hn_bgcolor = get_sub_field( 'kolor_tla' );
	}
	
	if (get_sub_field( 'pierwszy_kolor_naglowkow' )) {
		$hn_fcolor = get_sub_field( 'pierwszy_kolor_naglowkow' );
	}
	if (get_sub_field( 'drugi_kolor_naglowkow' )) {
		$hn_scolor = get_sub_field( 'drugi_kolor_naglowkow' );
	}
	
endwhile; endif;
?> 