<?php if( have_rows('slider-content') ): ?>
  <section class="circle">

    <!-- partial:index.partial.html -->
    <div class="slider" id="slider-1">
      <?php while ( have_rows('slider-content') ) : the_row(); ?>
        <div class="item">
          <svg xmlns="http://www.w3.org/2000/svg" class="original">
            <image width="100%" height="100%" preserveAspectRatio="xMidYMid slice" xlink:href="  <?php echo wp_get_attachment_image_url( get_sub_field('grafika'), "full", "", array() );  ?>" mask="url(#donutmask)"></image>
          </svg>
          <div class="tit">
            <h3><?php the_sub_field('naglowek'); ?></h3>
            <!-- <?php if ( have_rows( 'przycisk' ) ) : while ( have_rows( 'przycisk' ) ) : the_row(); ?>
              <div class="pos-button">
                <a href="<?php the_sub_field( 'link' ); ?>"><?php the_sub_field( 'napis' ); ?></a>
              </div>
              <?php endwhile; endif; ?> -->
          </div>
        </div>
      <?php endwhile; ?>
    </div>
    <svg>
      <defs>
        <mask id="donutmask">
          <circle id="outer" cx="250" cy="250" r="400" fill="white"/>
          <circle id="inner" cx="250" cy="250" r="300"/>
        </mask>
      </defs>
    </svg>

    <div class="cursor"></div>
    <!-- partial -->
  </section>
  <?php endif; ?>