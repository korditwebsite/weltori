<?php 
if ($hn_ulozenie == 1) {
	add_css_theme("choose-1", "/templates/hero/choose/choose-1/scss/style.css");
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row();
		if ( have_rows( 'wybierz_jedno' ) ) : while ( have_rows( 'wybierz_jedno' ) ) : the_row();
			get_template_part( '/templates/hero/choose/choose-1/init'); 
		endwhile; endif;
	endwhile; endif;
}
elseif ($hn_ulozenie == 2) {
	add_css_theme("choose-2", "/templates/hero/choose/choose-2/scss/style.css");
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row();
		if ( have_rows( 'wybierz_jedno' ) ) : while ( have_rows( 'wybierz_jedno' ) ) : the_row();
			get_template_part( '/templates/hero/choose/choose-2/init'); 
		endwhile; endif;
	endwhile; endif;
}
elseif ($hn_ulozenie == 3) {
	add_css_theme("choose-3", "/templates/hero/choose/choose-3/scss/style.css");
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row();
		get_template_part( '/templates/hero/choose/choose-3/init'); 
		wp_enqueue_script( 'choose-js', '/wp-content/themes/' . wp_get_theme() . '/templates/hero/choose/choose-3/js/main.js');
	endwhile; endif;
}
