<section class="presentation-logotype presentation-slider">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>);"></div>
	<div class="section-background-image wow fadeIn" style="background-image: url(<?php echo id_subimage_url("logotyp", "full") ?>);">
		
	</div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="position-content-box">
			<div class="content-logotype">

				<div class="checked-default">
					<h3 class="wow fadeIn"><?php the_sub_field( 'tytul' ); ?></h3>
				</div>
				<div class="number wow jackInTheBox">
					<?php the_sub_field( 'numer' ); ?>
				</div>
				<div class="sygnet wow jackInTheBox">
					<?php echo id_subimage("sygnet", "full") ?>
				</div>
				<?php if (get_sub_field( 'podpis' )): ?>
					<div class="checked-text">
						<a href="<?php the_sub_field( 'link' ); ?>">
							<img src="/wp-content/uploads/2019/12/arrow.svg" alt="" class="arow wow bounce infinite">
						</a>
						<div class="text wow fadeIn"><?php the_sub_field( 'podpis' ); ?></div>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</section>


