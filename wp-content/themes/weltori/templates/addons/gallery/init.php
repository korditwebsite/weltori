<?php 

//Size container
//var
$crop = get_sub_field( 'crop_thumbnail' );
$cropsizedata = get_sub_field( 'crop_to' );
$value = get_sub_field( 'value' );
$cropsize = "height:" . $cropsizedata . $value . ";";
$images = get_sub_field('galeria');
$size = 'gallery-home';
$size2 = 'hero_image';

if ($crop) {
	$cropclass = "crop-image";
} else {
	$cropclass = " ";
}
?>

<section class="gallery" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="row">
			<div class="col-xl-12 text-center">
				<h2><?php the_sub_field("tytul"); ?></h2>
				<p><?php the_sub_field("opis"); ?></p>
			</div>
			<?php 
			if( $images ): ?>
				<?php $time = 0; foreach( $images as $image ): ?>
				<figure class="<?php echo $hn_rwd; ?>">
					<a class="wow fadeInUp js-smartPhoto" data-caption="<?php echo $image['description'] ?>" href="<?php echo wp_get_attachment_image_url( $image['ID'], $size2 ); ?>">
						<img style="<?php echo $cropsize; ?>" class="img-fluid item-gallery <?php echo $cropclass; ?>" alt="item-gallery" src="<?php echo wp_get_attachment_image_url( $image['ID'], $size ); ?>" class="img-fluid">
					</a>
				</figure>
				<?php $time = $time +250; endforeach; ?>
			<?php endif;  ?>
		</div>
	</div>
</section>