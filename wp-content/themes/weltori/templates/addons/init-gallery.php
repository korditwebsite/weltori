<?php
$number = 1;
$number = get_sub_field( 'uklad' );

if ($number == 1) {
	get_template_part('/templates/addons/gallery/gallery');
}
elseif ($number == 2) {
	add_css_theme("masonry",  '/templates/addons/masonry/scss/style.css');
	get_template_part('/templates/addons/masonry/masonry');
	wp_enqueue_script( "init", get_template_directory_uri() . "/templates/addons/masonry/js/init.js", array(), '1.0.0', true );
}
