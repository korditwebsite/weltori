<section class="employees"  style="background-color:<?php echo $hn_bgcolor; ?>;">
    <div class="<?php echo $hn_size_container; ?>">
        <div class="row">
            <div class="col-lg-12">
                <h2><?php the_sub_field( 'tytul' ); ?></h2>
            </div>
        </div>
        <div class="row">
            <?php if( have_rows('team_box') ): while( have_rows('team_box') ): the_row(); ?>
                <div class="<?php echo $hn_rwd; ?>">
                    <div class="employees-wrapper">
                        <?php echo id_subimage("grafika","big"); ?>
                        <div class="employees-box">
                            <h4><?php the_sub_field("imie_i_nazwisko"); ?></h4>
                            <p class="position"><?php the_sub_field("stanowisko"); ?></p>
                            <?php if (get_sub_field("numer_telefonu")): ?>
                                <a href="tel:+48:<?php the_sub_field("numer_telefonu"); ?>">
                                    <?php exportsvg("/assets/img/phone.svg"); ?>
                                    <?php the_sub_field("numer_telefonu"); ?>
                                </a>
                            <?php endif ?>
                            <?php if (get_sub_field("mail")): ?>
                                <a href="mailto:<?php the_sub_field("mail"); ?>">
                                    <?php 
                                    exportsvg("/assets/img/mail.svg");
                                    ?>
                                    <?php the_sub_field("mail"); ?>
                                </a>
                            <?php endif ?>
                        </div>
                        <?php if (get_sub_field("opis")): ?>
                            <div class="text-employees">
                                <?php the_sub_field("opis"); ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>

