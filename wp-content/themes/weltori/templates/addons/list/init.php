<section class="list-section" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="title-container text-center">
			<h3 style="color: <?php echo $hn_fcolor; ?>;"><?php the_sub_field("title"); ?></h3>
			<p style="color: <?php echo $hn_fcolor; ?>;"><?php the_sub_field("subtitle"); ?></p>
		</div>
		<ul class="row">
			<?php if ( have_rows( 'lista' ) ) : while ( have_rows( 'lista' ) ) : the_row(); ?>
				<li class="<?php echo $hn_rwd; ?>" style="color:<?php echo $hn_scolor; ?>">
					<?php the_sub_field( 'wartosc' ); ?>
				</li>
			<?php endwhile; endif; ?>
		</ul>
	</div>
</section>


