<?php get_header(); ?>
<main id="single-post">
	<?php
	if (get_post_type() === 'portfolio') {
		add_css_theme("contact-1-css", "/templates/single-post/portfolio/scss/style.css");
		get_template_part( 'templates/single-post/portfolio/portfolio'); 
	}

	elseif (get_post_type() === 'galeria') {
		get_template_part( 'templates/single-post/gallery'); 
	}

	elseif (get_post_type() === 'service' ) {
		get_template_part( 'templates/single-post/service'); 
	}

	elseif (get_post_type() === 'post') {
		get_template_part( 'templates/blog/blog'); 
	}
	?>
</main>
<?php get_footer(); ?>

