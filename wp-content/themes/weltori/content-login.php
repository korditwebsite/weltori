<?php
/**
* Template Name: login page
*/
?>
<head>
	<?php
	// if (is_user_logged_in()) {
	// 	if ( (isset($_GET['action']) && $_GET['action'] != 'logout') || (isset($_POST['login_location']) && !empty($_POST['login_location'])) ) {
	// 		add_filter('login_redirect', 'my_login_redirect', 10, 3);
	// 		function my_login_redirect() {
	// 			$location = $_SERVER['HTTP_REFERER'];
	// 			wp_safe_redirect($location);
	// 			exit();
	// 		}
	// 	}
	// } ?>
	<?php wp_head(); ?>
	<style type="text/css" src="/wp-content/themes/hypernova/assets/sass/_general.css"></style>
</head>

<style type="text/css">
	@font-face {
		font-family: "Didot";
		src: url("/wp-content/themes/weltori/assets/fonts/didot.ttf");
	}
	@font-face {
		font-family: "Acenir";
		src: url("/wp-content/themes/weltori/assets/fonts/Avenir.ttc");
	}
	body.login {
		background: url(/wp-content/uploads/2019/12/bg-login.jpg);
		background-size: cover;
		display: flex;
		justify-content: center; 
		align-items: center; 
	}
	body.login input[type=password], body.login input[type=text] {
		font-size: unset !important;
		min-height: inherit !important;
		border: none;
		border-bottom: solid 1px #ada7a0;
		background: transparent;
		border-radius: 0px; 
	}
	body.login div#login h1 {
		display: none; 
	}
	body.login div#login a {
		color: #fff; 
	}
	body.login .container-login {
		width: 40vw;
		max-width: 90vw;
		background-image: linear-gradient(#f6f1e8, #f6f1e8);
		display: flex;
		flex-wrap: wrap;
		padding-bottom: 0px; 
		padding: 20px;
	}
	body.login form p {
		margin-bottom: 20px;
		display: flex;
		align-items: baseline;
	}
	body.login form p:nth-child(1), body.login form p:nth-child(2) {
		width: 100%; 
	}
	body.login form p:nth-child(3), body.login form p:nth-child(4) {
		width: 50%; 
	}
	body.login form p:nth-child(3) {
		display: flex;
		justify-content: flex-start; 
		margin-bottom: 0px;
		font-size: 10px;
	}
	body.login form input#rememberme {
		margin-bottom: 5px !important;
	}
	body.login form p:nth-child(4) {
		display: flex;
		justify-content: flex-end; 
		margin-bottom: 0px;
		flex-direction: row-reverse;
	}
	body.login form p:nth-child(1) > *:nth-child(1), body.login form p:nth-child(2) > *:nth-child(1) {
		width: 40%; 
		font-size:12px;
		padding-right: 10px;
	}
	body.login form p input#wp-submit, body.login form p a.anuluj {
		background: #a67920;
		border: none;
		border-radius: 20px;
		padding: 5px 10px;
		text-transform: uppercase;
		letter-spacing: 2px;
		color: #fff;
		font-size: 11px;
		text-align: center;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	body.login form p a.anuluj {
		margin-right: 10px;
	}
	body.login form input {
		margin-bottom: 0px !important; 
		width:70%;
	}
	body.login p.d-block {
		width: 100%;
	}
	body.login form#loginform {
		display: flex;
		flex-wrap: wrap;
		margin-bottom: 0px;
	}
	body.login h2 {
		font-family: "Didot";
		font-weight: 700;
	}

	/*# sourceMappingURL=_general.css.map */

</style>
<body class="login">
	<div class="container-login">
		<h2>zaloguj</h2>
		<div class="pl-3">
			<p class="d-block">weltori.pl</p>
			<?php wp_login_form( array('redirect' => wp_get_referer()) ); ?>
		</div>
	</div>
	<?php wp_footer(); ?>
	<script type="text/javascript">
		var bool = "<?php echo wp_get_referer() ?>"; 
		$('#loginform p.login-submit').append('<a class="anuluj" href="' + bool +'">anuluj</a>');
	</script>
</body>